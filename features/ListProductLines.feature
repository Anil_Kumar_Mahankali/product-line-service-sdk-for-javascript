Feature: List product lines
  Lists all product lines

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listProductLines
    Then all product lines in the product-line-service are returned