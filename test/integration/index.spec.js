import ProductLineServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be ProductLineServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new ProductLineServiceSdk(config.productLineServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(ProductLineServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('listProductLines method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ProductLineServiceSdk(config.productLineServiceSdkConfig);


                /*
                 act
                 */
                const productLinesPromise =
                    objectUnderTest
                        .listProductLines(
                            factory
                                .constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */

                productLinesPromise
                    .then((productLines) => {
                        expect(productLines.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            })
        });
    }, 30000);
});